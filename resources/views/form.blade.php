<!DOCTYPE html>
<html>
<body>

<h1>Buat Account Baru!</h1>
<h2>SIgn Up Form</h2>
<form action="/welcome" method="POST" >
  @csrf
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" ><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" ><br><br>
  <p>Gender:</p>
  <input type="radio" id="male" name="gender" value="male">
  <label for="male">Male</label><br>
  <input type="radio" id="female" name="gender" value="female">
  <label for="female">Female</label><br>
  <br>
  <label for="Nationality">Nationality:</label>
  <br>
  <select name="Indonesian" id="Indonesian">
    <option value="Indonesian">Indonesian</option>
    <option value="Malaysian">Malaysian</option>
    <option value="Singapore">Singapore</option>
    <option value="Australia">Australia</option>
  </select>
  <p>Language Spoken:</p>
  <input type="checkbox" id="BahasaIndonesia" name="BahasaIndonesia" value="BahasaIndonesia">
  <label for="BahasaIndonesia"> BahasaIndonesia</label><br>
  <input type="checkbox" id="English" name="English" value="English">
  <label for="English"> English</label><br>
  <input type="checkbox" id="Other" name="Other" value="Other">
  <label for="Other">Other</label><br><br>
  <p>Bio:</p>
  <textarea rows="7" cols="25" name="comment" form="usrform"></textarea><br>
  <a href=>
    <input type="submit" value="Submit">
  </a>
</form>

</body>
</html>